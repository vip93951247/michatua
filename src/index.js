"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var io = require("socket.io");
var http = require("http");
var actions_1 = require("./actions");
var routing_1 = require("./routing");
var mongo_1 = require("./mongo");
var constants_1 = require("./constants");
var db_1 = require("./config/db");
exports.app = express();
var server = http.createServer(exports.app);
var connection = mongo_1.MongoClient.connect(db_1.url, function (err, client) {
    if (err) {
        return console.log(err);
    }
    server.listen(constants_1.PORT, function () {
        console.log('Подключен на порту: ' + constants_1.PORT);
    });
    var db = client.db('chat_dev');
    var socketsIo = io.listen(server);
    routing_1.routes(exports.app, db);
    actions_1.events(socketsIo, db);
});
