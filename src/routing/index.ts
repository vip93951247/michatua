import * as path from "path";

export const routes = function (app, db) {
    app.get('/', function(request, response) {
        response.sendFile(path.resolve('src/templates/index.html'));
    });
};


export default routes;