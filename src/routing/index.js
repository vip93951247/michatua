"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
exports.routes = function (app, db) {
    app.get('/', function (request, response) {
        response.sendFile(path.resolve('src/templates/index.html'));
    });
};
exports.default = exports.routes;
