import * as express from "express";
import * as io from 'socket.io';
import * as http from "http";
import {events} from "./actions";
import {routes} from "./routing";
import {MongoClient} from "./mongo";
import {PORT} from "./constants";
import {url} from "./config/db";

export const app = express();

const server = http.createServer(app);

const connection = MongoClient.connect(url, (err, client) => {
    if (err) {
        return console.log(err)
    }

    server.listen(PORT, () => {
        console.log('Подключен на порту: ' + PORT);
    });

    let db = client.db('chat_dev');
    let socketsIo = io.listen(server);

    routes(app, db);
    events(socketsIo, db);
});

