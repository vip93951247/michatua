"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var connections = [];
exports.events = function (socketsIo, db) {
    socketsIo.sockets.on('connection', function (socket) {
        console.log("Успешное соединение");
        connections.push(socket);
        socket.on('disconnect', function (data) {
            connections.splice(connections.indexOf(socket), 1);
            console.log("Отключились");
        });
        socket.on('send mess', function (data) {
            var values = {
                mess: data.mess,
                name: data.name,
                className: data.className
            };
            socketsIo.sockets.emit('add mess', values);
            db.collection('messages').insertOne(values, function (err, results) {
                if (err) {
                    console.error(err);
                }
                console.log('Сообщение отправлено');
            });
        });
    });
};
exports.default = exports.events;
