import * as mongoDB from "mongodb";

export const MongoClient = mongoDB.MongoClient;
export default MongoClient;